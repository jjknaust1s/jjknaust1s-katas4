const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";
const lotrCitiesArray = ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"];
const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";

// let header = document.createElement("div");
// header.textContent = "Kata 0";
// document.body.appendChild(header);

// function kata0() {
//     let newElement = document.createElement("div");
//     newElement.textContent = JSON.stringify(lotrCitiesArray);
//     document.body.appendChild(newElement)
//     return lotrCitiesArray; // Don't forget to return your output!
// }
// kata0()

// 1.)
function kata1() {
    let citiesFromGot = []
    cities = gotCitiesCSV.split(',')
    for (let i = 0; i < cities.length; i++) {
        citiesFromGot.push(cities[i])
    }
    return citiesFromGot
}
const kataResult1 = document.createTextNode(kata1())
const breakTag1 = document.createElement('br')
document.body.appendChild(kataResult1)
document.body.appendChild(breakTag1)
    // 2.)
function kata2() {
    let thingsArray = []
    let bestThingArray = bestThing.split(' ')
    for (let i = 0; i < bestThingArray.length; i++) {
        thingsArray.push(cities[i])
    }
    return bestThingArray
}
let brk = document.createElement('br')
const divide = document.createTextNode(brk)
const kataResult2 = document.createTextNode(kata2())
document.body.appendChild(brk)
document.body.appendChild(kataResult2)
    // 3.)
function kata3() {
    let gotSemiColon = gotCitiesCSV.split(',').join(' ; ')
    return gotSemiColon
}
let brk2 = document.createElement('br')
const divide2 = document.createTextNode(brk2)
const kataResult3 = document.createTextNode(kata3())
document.body.appendChild(brk2)
document.body.appendChild(kataResult3)
    // 4.)
function kata4() {
    let strLotrCities = lotrCitiesArray.join()
    return strLotrCities
}
let brk3 = document.createElement('br')
const divide3 = document.createTextNode(brk3)
const kataResult4 = document.createTextNode(kata4())
document.body.appendChild(brk3)
document.body.appendChild(kataResult4)
    // 5.)
function kata5() {
    let first5LotrCities = []
    for (let i = 0; i <= 4; i++) {
        first5LotrCities.push(lotrCitiesArray[i])
    }
    return first5LotrCities
}
let brk4 = document.createElement('br')
const divide4 = document.createTextNode(brk4)
const kataResult5 = document.createTextNode(kata5())
document.body.appendChild(brk4)
document.body.appendChild(kataResult5)
    // 6.)
function kata6() {
    let last5LotrCities = []
    for (let i = 3; i <= 7; i++) {
        last5LotrCities.push(lotrCitiesArray[i])
    }
    return last5LotrCities
}
let brk5 = document.createElement('br')
const divide5 = document.createTextNode(brk5)
const kataResult6 = document.createTextNode(kata6())
document.body.appendChild(brk5)
document.body.appendChild(kataResult6)
    // 7.)
function kata7() {
    let lotr3rdThru5thCities = lotrCitiesArray.slice(2, 5)
    return lotr3rdThru5thCities
}
let brk6 = document.createElement('br')
const divide6 = document.createTextNode(brk6)
const kataResult7 = document.createTextNode(kata7())
document.body.appendChild(brk6)
document.body.appendChild(kataResult7)
    // 8.)
function kata8() {
    let withoutRohan = lotrCitiesArray.splice(2, 1)
    return lotrCitiesArray
}
let brk7 = document.createElement('br')
const divide7 = document.createTextNode(brk7)
const kataResult8 = document.createTextNode(kata8(lotrCitiesArray))
document.body.appendChild(brk7)
document.body.appendChild(kataResult8)
    // 9.)
function kata9() {
    let nothingAfterDeadMarshes = lotrCitiesArray.splice(5, 2)
    return lotrCitiesArray
}
let brk8 = document.createElement('br')
const divide8 = document.createTextNode(brk8)
const kataResult9 = document.createTextNode(kata9(lotrCitiesArray))
document.body.appendChild(brk8)
document.body.appendChild(kataResult9)
    // 10.)
function kata10() {
    let rohansBack = lotrCitiesArray.splice(2, 0, 'Rohan')
    return lotrCitiesArray
}
let brk9 = document.createElement('br')
const divide9 = document.createTextNode(brk9)
const kataResult10 = document.createTextNode(kata10(lotrCitiesArray))
document.body.appendChild(brk9)
document.body.appendChild(kataResult10)
    // 11.)
function kata11() {
    let deadToDeadest = lotrCitiesArray.splice(5, 1, 'Deadest Marshes')
    return lotrCitiesArray
}
let brk10 = document.createElement('br')
const divide10 = document.createTextNode(brk10)
const kataResult11 = document.createTextNode(kata11(lotrCitiesArray))
document.body.appendChild(brk10)
document.body.appendChild(kataResult11)
    // 12.) it says bestthing(not bestThingArray) do spaces count as characters
function kata12() {
    let first14 = bestThing.slice(0, 14)
    return first14
}
let brk11 = document.createElement('br')
const divide11 = document.createTextNode(brk11)
const kataResult12 = document.createTextNode(kata12())
document.body.appendChild(brk11)
document.body.appendChild(kataResult12)
    // 13.) it says bestthing(not bestThingArray) do spaces count as characters
function kata13() {
    let last14 = bestThing.slice(-12)
    return last14
}
let brk12 = document.createElement('br')
const divide12 = document.createTextNode(brk12)
const kataResult13 = document.createTextNode(kata13())
document.body.appendChild(brk12)
document.body.appendChild(kataResult13)
    //     // 14.)
function kata14() {
    let twenty3rdToTwenty8th = bestThing.slice(23, 39)
    return twenty3rdToTwenty8th
}
let brk13 = document.createElement('br')
const divide13 = document.createTextNode(brk13)
const kataResult14 = document.createTextNode(kata14())
document.body.appendChild(brk13)
document.body.appendChild(kataResult14)
    // 15.)
function kata15() {
    let kata13DifferentWay = bestThing.substring(68, 81)
    return kata13DifferentWay
}
let brk14 = document.createElement('br')
const divide14 = document.createTextNode(brk14)
const kataResult15 = document.createTextNode(kata15())
document.body.appendChild(brk14)
document.body.appendChild(kataResult15)
    // 16.)
function kata16() {
    let kata14DifferentWay = bestThing.substring(23, 39)
    return kata14DifferentWay
}
let brk15 = document.createElement('br')
const divide15 = document.createTextNode(brk15)
const kataResult16 = document.createTextNode(kata16())
document.body.appendChild(brk15)
document.body.appendChild(kataResult16)
    // 17.)
function kata17() {
    let removeTheLastCity = lotrCitiesArray.pop()
    return lotrCitiesArray
}
let brk16 = document.createElement('br')
const divide16 = document.createTextNode(brk16)
const kataResult17 = document.createTextNode(kata17(lotrCitiesArray))
document.body.appendChild(brk)
document.body.appendChild(kataResult17)
    // 18.)
function kata18() {
    let addDeadestMarhes = lotrCitiesArray.push('Deadest Marshes')
    return lotrCitiesArray
}
let brk17 = document.createElement('br')
const divide17 = document.createTextNode(brk17)
const kataResult18 = document.createTextNode(kata18(lotrCitiesArray))
document.body.appendChild(brk17)
document.body.appendChild(kataResult18)
    //     // 19.)
function kata19() {
    let removeTheFirstCity = lotrCitiesArray.shift()
    return lotrCitiesArray
}
let brk18 = document.createElement('br')
const divide18 = document.createTextNode(brk18)
const kataResult19 = document.createTextNode(kata19(lotrCitiesArray))
document.body.appendChild(brk18)
document.body.appendChild(kataResult19)
    //     // 20.)
function kata20() {
    let removeTheFirstCity = lotrCitiesArray.unshift('Mordor')
    return lotrCitiesArray
}
let brk19 = document.createElement('br')
const divide19 = document.createTextNode(brk19)
const kataResult20 = document.createTextNode(kata20(lotrCitiesArray))
document.body.appendChild(brk19)
document.body.appendChild(kataResult20)


// // let header = document.createElement("div");
// // header.textContent = "Kata 0";
// // document.body.appendChild(header);

// // function kata0() {
// //     let newElement = document.createElement("div");
// //     newElement.textContent = JSON.stringify(lotrCitiesArray);
// //     document.body.appendChild(newElement)
// //     return lotrCitiesArray; // Don't forget to return your output!
// // }
// // return kata0()

// // let brk = document.createElement('br')
// // const divide3 = document.createTextNode(brk)
// // const kataResult = document.createTextNode(kata())
// // document.body.appendChild(brk)
// // document.body.appendChild(kataResult)